﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xsens;
using XDA;

namespace AwindaMonitor
{
    class DeviceData
    {
        public string _name { get; set; }
        public uint _Id { get; set; }
		public int _batteryLevel { get; set; }
		public int _rssi { get; set; }
		public int _effectiveUpdateRate { get; set; }
        public List<int> _frameSkipsList { get; set; }
        public uint _sumFrameSkips { get; set; }
        public DateTime _LastUpdate { get; set; }
        public double _freq { get; set; }

        public byte[] _dataBytes
        {
            get
            {
                List<byte> result = new List<byte>();
                //byte[] result = new byte[9];
                int i = 0;
                byte[] tempo = BitConverter.GetBytes(_Id);                      //0
                for (int iter = 0; iter < tempo.Length; iter++)
                {
                    result.Add(tempo[iter]);
                    i++;
                }
                tempo = BitConverter.GetBytes((float)_orientationQuat.w());     //4
                for (int iter = 0; iter < tempo.Length; iter++)
                {
                    result.Add(tempo[iter]);
                    i++;
                }
                tempo = BitConverter.GetBytes((float)_orientationQuat.x());     //8
                for (int iter = 0; iter < tempo.Length; iter++)
                {
                    result.Add(tempo[iter]);
                    i++;
                }
                tempo = BitConverter.GetBytes((float)_orientationQuat.y());     //12
                for (int iter = 0; iter < tempo.Length; iter++)
                {
                    result.Add(tempo[iter]);
                    i++;
                }
                tempo = BitConverter.GetBytes((float)_orientationQuat.z());     //16
                for (int iter = 0; iter < tempo.Length; iter++)
                {
                    result.Add(tempo[iter]);
                    i++;
                }
                for(uint coeff = 0; coeff < _acceleration.size(); coeff ++)     //20
                {
                    tempo = BitConverter.GetBytes((float)_acceleration.value(coeff));
                    for (int iter = 0; iter < tempo.Length; iter++)
                    {
                        result.Add(tempo[iter]);
                        i++;
                    }
                }
                for (uint coeff = 0; coeff < _angularVel.size(); coeff++)       //32
                {
                    tempo = BitConverter.GetBytes((float)_angularVel.value(coeff));
                    for (int iter = 0; iter < tempo.Length; iter++)
                    {
                        result.Add(tempo[iter]);
                        i++;
                    }
                }
                /*for (uint coeff = 0; coeff < _magneticF.size(); coeff++)        //44
                {
                    tempo = BitConverter.GetBytes((float)_magneticF.value(coeff));
                    for (int iter = 0; iter < tempo.Length; iter++)
                    {
                        result.Add(tempo[iter]);
                        i++;
                    }
                }*/
                tempo = BitConverter.GetBytes(_effectiveUpdateRate);            //56
                for (int iter = 0; iter < tempo.Length; iter++)
                {
                    result.Add(tempo[iter]);
                    i++;
                }
                return result.ToArray();                                        //60

            }
        }
        private XsVector _angVel;
        public XsVector _angularVel 
        {
            get
            {
                return _angVel;
            }

            set
            {
                _angVel = value;
                _containsAngVel = true;
            }
        }

        private XsQuaternion _oriQuat;
        public XsQuaternion _orientationQuat
        {
            get
            {
                return _oriQuat;
            }

            set
            {
                _oriQuat = value;
                _containsOrientationQuat = true;
            }
        }

        private XsVector _acc;
        public XsVector _acceleration
        {
            get
            {
                return _acc;
            }

            set
            {
                _acc = value;
                _containsOrientationAcc = true;
            }
        }

        private XsVector _mag;
        public XsVector _magneticF
        {
            get
            {
                return _mag;
            }

            set
            {
                _mag = value;
                _containsMagneticF = true;
            }
        }

        private XsEuler _ori;
        public XsEuler _orientation
        {
            get
            {
                return _ori;
            }
            
            set
            {
                _ori = value;
                _containsOrientation = true;
            }
        }

        public bool _containsOrientation { get; set; }
        public bool _containsOrientationQuat { get; set; }
        public bool _containsOrientationAcc { get; set; }
        public bool _containsMagneticF { get; set; }
        public bool _containsAngVel { get; set; }
		
		public DeviceData()
		{
            _Id = 0;
            _name = "";
            _batteryLevel = 0;
            _rssi = 0;
            _effectiveUpdateRate = 0;
            _frameSkipsList = new List<int>();
            _sumFrameSkips = 0;
            _ori = new XsEuler(0.0, 0.0, 0.0);
            _containsOrientation = false;
            _oriQuat = new XsQuaternion(0.0, 0.0, 0.0, 0.0);
            _containsOrientationQuat = false;
            _acc = new XsVector();
            _containsOrientationAcc = false;
            _LastUpdate = DateTime.Now;
		}

        public DeviceData(DeviceData copie)
        {
            _Id = copie._Id;
            _name = copie._name;
            _batteryLevel = copie._batteryLevel;
            _rssi = copie._rssi;
            _effectiveUpdateRate = copie._effectiveUpdateRate;
            _frameSkipsList = new List<int>(copie._frameSkipsList);
            _sumFrameSkips = copie._sumFrameSkips;
            _ori = copie._ori;
            _containsOrientation = copie._containsOrientation;
            _oriQuat = copie._oriQuat;
            _containsOrientationQuat = copie._containsOrientationQuat;
            _acc = copie._acc;
            _containsOrientationAcc = copie._containsOrientationAcc;
            _LastUpdate = copie._LastUpdate;
            _angularVel = copie._angularVel;
            _mag = copie._mag; 
        }

        
    }
}
