﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xsens;
using XDA;

namespace AwindaMonitor
{
    class DeviceManager
    {
        enum State {CONNECTED, LOWBATTERY, DISCONNECTED}
        private List<uint> knownDevices = new List<uint>();
        private Dictionary<uint, DeviceData> dicoData = new Dictionary<uint, DeviceData>();
        private Dictionary<uint, State> dicoState = new Dictionary<uint, State>();
        private bool disconnected = false; // is there at least one disconnected IMU
        private bool connected = false; // is there at least one connected IMU
        private bool lowBatery = false; // is there at least one lowBattery IMU

        public List<uint> updatedDevices = new List<uint>();

        public List<uint> getConnectedDevices()
        {
            List<uint> result = new List<uint>();
            foreach (uint IMU in knownDevices)
            {
                if (dicoState[IMU] != State.DISCONNECTED)
                {
                    result.Add(IMU);
                }
            }
            return result;
        }
        public List<uint> getDisconnectedDevices()
        {
            List<uint> result = new List<uint>();
            foreach (uint IMU in knownDevices)
            {
                if (dicoState[IMU] == State.DISCONNECTED)
                {
                    result.Add(IMU);
                }
            }
            return result;
        }
        public List<uint> getLowBatteryDevices()
        {
            List<uint> result = new List<uint>();
            foreach (uint IMU in knownDevices)
            {
                if (dicoState[IMU] == State.LOWBATTERY)
                {
                    result.Add(IMU);
                }
            }
            return result;
        }

        public void reset()
        {
            knownDevices.Clear();
            dicoData.Clear();
            dicoState.Clear();
            disconnected = false;
            connected = false;
            lowBatery = false;
        }

        public void addDevice(uint IMUid)
        {
            knownDevices.Add(IMUid);
            if (!dicoData.ContainsKey(IMUid))
                dicoData.Add(IMUid, new DeviceData());
            if (!dicoState.ContainsKey(IMUid))
                dicoState.Add(IMUid, State.CONNECTED);
        }
        public bool checkDevice(uint IMU)
        {
            if (knownDevices.Contains(IMU))
            {
                return true;
            }
            else
            {
                addDevice(IMU);
                return false;
            }
        }
        public void checkDisconnection(DateTime _time)
        {
            foreach (uint IMU in knownDevices)
            {
                double tempo = _time.Subtract(dicoData[IMU]._LastUpdate).TotalMilliseconds;
                if (tempo > 1000)
                {
                    if (!getDisconnectedDevices().Contains(IMU))
                    {
                        disconnect(IMU);
                    }
                }
                else
                {
                    if (getDisconnectedDevices().Contains(IMU))
                    {
                        connect(IMU);
                    }
                }
            }
        }
        public bool isKnown(uint IMU)
        {
            return knownDevices.Contains(IMU);
        }

        public bool isDisconnected() { return disconnected; }
        public bool isDisconnected(uint IMU)
        {
            return dicoState[IMU] == State.DISCONNECTED;
        }
        public bool isLowBattery() { return lowBatery; }
        public bool isLowBattery(uint IMU)
        {
            return dicoState[IMU] == State.LOWBATTERY;
        }
        public bool isConnected() { return connected; }
        public bool isConnected(uint IMU)
        {
            return dicoState[IMU] == State.CONNECTED;
        }

        public void disconnect(uint IMU)
        {
            dicoState[IMU] = State.DISCONNECTED;
            updateStates();
        }
        public void connect(uint IMU)
        {
            if (dicoState[IMU] == State.DISCONNECTED)
                dicoState[IMU] = State.CONNECTED;
            updateStates();
        }
        public void LowBatt(uint IMU)
        {
            dicoState[IMU] = State.LOWBATTERY;
            updateStates();
        }

        private void updateStates()
        {
            lowBatery = false;
            disconnected = false;
            connected = false;
            foreach (uint IMU in knownDevices)
            {
                switch (dicoState[IMU])
                {
                    case State.LOWBATTERY:
                        {
                            lowBatery = true;
                            break;
                        }
                    case State.CONNECTED:
                        {
                            connected = true;
                            break;
                        }
                    case State.DISCONNECTED:
                        {
                            disconnected = true;
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
            }
        }

        public DeviceData getData(uint IMU)
        {
            uint test = IMU;
            foreach ( uint truc  in dicoData.Keys)
            {
                uint test2 = truc;
            }
            return dicoData[IMU];
        }
        public void setData(uint IMU, DeviceData data)
        {
            dicoData[IMU] = data;
        }

    }
}
