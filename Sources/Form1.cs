/*	Copyright (c) 2003-2016 Xsens Technologies B.V. or subsidiaries worldwide.
	All rights reserved.

	Redistribution and use in source and binary forms, with or without modification,
	are permitted provided that the following conditions are met:

	1.	Redistributions of source code must retain the above copyright notice,
		this list of conditions and the following disclaimer.

	2.	Redistributions in binary form must reproduce the above copyright notice,
		this list of conditions and the following disclaimer in the documentation
		and/or other materials provided with the distribution.

	3.	Neither the names of the copyright holders nor the names of their contributors
		may be used to endorse or promote products derived from this software without
		specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
	THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
	OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
	HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY OR
	TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Windows.Threading;
using System.Diagnostics;
using Xsens;
using XDA;

namespace AwindaMonitor
{
	public partial class Form1 : Form
	{
		public enum States {
			DETECTING,
			CONNECTING,
			CONNECTED,
			ENABLED,
			OPERATIONAL,
			AWAIT_MEASUREMENT_START,
			MEASURING,
            SENDING
			//AWAIT_RECORDING_START,
			//RECORDING,
			//FLUSHING
		};

        static private States _state;
        static public States State { get { return _state; } }

        private static Form1 instance;
        public static Form1 Instance
        {
            get
            {
                return instance;
            }
        }
		XsDevice _MyWirelessMasterDevice;

		private System.Windows.Forms.Timer _portScanTimer;
		private System.Windows.Forms.Timer _batteryLevelRequestTimer;
		
		// Connected MTw's
		//internal List<XsDevice> _mtws = new List<XsDevice>();
        private bool isAwindaStation = true;

		private MyXda _myxda;
        private DeviceManager manager;
		// Callback handler to track connectivity and data
		private MyWirelessMasterCallback m_myWirelessMasterCallback;
		private Dictionary<XsDevice, MyMtwCallback> _measuringMtws;
		private Dictionary<XsDevice, MyMtwCallback>.Enumerator _nextBatteryRequest;
        //private Dictionary<uint, string> _disconnectedIDs;

        //SerialCommunication serialComm;
        private CommunicationSocket remoteCommunication;
        public bool communicationPortIsOpen = false;
        public bool communicationActive = false;
        //public const string RemoteConnectionDescription = "Type: <mikey>.selectedRemoteComType = 0 for Serial Port and <mikey>.selectedRemoteComType = 1 for UDP port";
        //public enum RemoteCommunicationType { Serial = 0, UDP = 1 };
        public bool UDPfastRead = true;
        private DateTime LastSent = DateTime.Now;
        private DateTime LastCheckBattery = DateTime.Now;
        private DateTime TimeLastCheckDeco = DateTime.Now;
        private List<uint> redDevices = new List<uint>();
        private List<uint> greenDevices = new List<uint>();
        private List<XsDeviceId> alreadyKnowDevices = new List<XsDeviceId>();

        private bool warningDisplayed = false;

        //GUI
        private int batteryWarning = 10;

        private bool SendAllOfThem = false;

		public Form1()
		{
			InitializeComponent();

            manager = new DeviceManager();

			_measuringMtws = new Dictionary<XsDevice, MyMtwCallback>();

			// create xda instance and set up callback handling
			_myxda = new MyXda();
			_myxda.WirelessMasterDetected += new EventHandler<PortInfoArg>(_myxda_WirelessMasterDetected);
			_myxda.DockedMtwDetected += new EventHandler<PortInfoArg>(_myxda_DockedMtwDetected);
			_myxda.MtwUndocked += new EventHandler<PortInfoArg>(_myxda_MtwUndocked);
			_myxda.OpenPortSuccessful += new EventHandler<PortInfoArg>(_myxda_OpenPortSuccessful);
			_myxda.OpenPortFailed += new EventHandler<PortInfoArg>(_myxda_OpenPortFailed);
			
			m_myWirelessMasterCallback = new MyWirelessMasterCallback();
			m_myWirelessMasterCallback.MtwWireless += new EventHandler<DeviceIdArg>(_callbackHandler_MtwWireless);                 
			m_myWirelessMasterCallback.MtwDisconnected += new EventHandler<DeviceIdArg>(_callbackHandler_MtwDisconnected);      
			m_myWirelessMasterCallback.MeasurementStarted += new EventHandler<DeviceIdArg>(_callbackHandler_MeasurementStarted);      
			m_myWirelessMasterCallback.MeasurementStopped += new EventHandler<DeviceIdArg>(_callbackHandler_MeasurementStopped);      
			m_myWirelessMasterCallback.DeviceError += new EventHandler<DeviceErrorArgs>(_callbackHandler_DeviceError);      

			_portScanTimer = new System.Windows.Forms.Timer();
			_portScanTimer.Interval = 1000;
			_portScanTimer.Tick += new EventHandler(scanPorts);

			_batteryLevelRequestTimer = new System.Windows.Forms.Timer();
			_batteryLevelRequestTimer.Interval = 1000;
			_batteryLevelRequestTimer.Tick += new EventHandler(requestBatteryLevels);

			_state = States.DETECTING;
			log("Detecting...");
			setWidgetsStates();

			comboBoxChannel.SelectedIndex = 0;

			_portScanTimer.Enabled = true;
			_batteryLevelRequestTimer.Enabled = true;
            instance = this;

            //View newView = connectedMtwList.View;
		}

		private void requestBatteryLevels(object sender, EventArgs e)
		{
			if (_measuringMtws.Count == 0)
				return;
			// It is impossible to request battery status for all MTWs at once. So cycle between them
			if (!_nextBatteryRequest.MoveNext()) 
			{
				_nextBatteryRequest = _measuringMtws.GetEnumerator();
			}
			else
				_nextBatteryRequest.Current.Key.requestBatteryLevel();
		}

		private void scanPorts(object sender, EventArgs e)
		{
			Thread thread = new Thread(_myxda.scanPorts);
			thread.Start();
		}

		void _myxda_WirelessMasterDetected(object sender, PortInfoArg e)
		{
			if (InvokeRequired)
			{
				// Update UI, make sure this happens on the UI thread
				BeginInvoke(new Action(delegate { _myxda_WirelessMasterDetected(sender, e); }));
			}
			else
			{
				switch (_state)
				{
				case States.DETECTING:
					{
						log(String.Format("Master Detected. Port: {0}, ID: {1}", e.PortInfo.portName().toString(), e.PortInfo.deviceId().toXsString().toString()));
						_state = States.CONNECTING;
                        if(e.PortInfo.deviceId().isAwindaDongle())
                        {
                            dockedMtwList.Items.Add("No Awinda Station Connected");
                            isAwindaStation = false;
                        }
						_myxda.openPort(e.PortInfo);
					} break;

				default:
					break;
				}
			}

			setWidgetsStates();
		}
		void _myxda_DockedMtwDetected(object sender, PortInfoArg e)
		{
            if (!isAwindaStation)
                return;
			if (InvokeRequired)
			{
				// Update UI, make sure this happens on the UI thread
				BeginInvoke(new Action(delegate { _myxda_DockedMtwDetected(sender, e); }));
			}
			else
			{
				log(String.Format("MTw Docked. Port: {0}, ID: {1}", e.PortInfo.portName().toString(), e.PortInfo.deviceId().toXsString().toString()));

				String mtwId = e.PortInfo.deviceId().toXsString().toString();
				if (dockedMtwList.FindStringExact(mtwId) == ListBox.NoMatches)
				{
					dockedMtwList.Items.Add(mtwId);
				}
				dockedMtwListGroupBox.Text = String.Format("Docked MTw list ({0}):", dockedMtwList.Items.Count);
			}
		}
		void _myxda_MtwUndocked(object sender, PortInfoArg e)
		{
            if (!isAwindaStation)
                return;
			if (InvokeRequired)
			{
				// Update UI, make sure this happens on the UI thread
				BeginInvoke(new Action(delegate { _myxda_MtwUndocked(sender, e); }));
			}
			else
			{
				log(String.Format("MTw Undocked. Port: {0}, ID: {1}", e.PortInfo.portName().toString(), e.PortInfo.deviceId().toXsString().toString()));

				String mtwId = e.PortInfo.deviceId().toXsString().toString();

				dockedMtwList.Items.Remove(mtwId);
				dockedMtwListGroupBox.Text = String.Format("Docked MTw list ({0}):", dockedMtwList.Items.Count);
			}
		}
		void _myxda_OpenPortSuccessful(object sender, PortInfoArg e)
		{
			if (InvokeRequired)
			{
				// Update UI, make sure this happens on the UI thread
				BeginInvoke(new Action(delegate { _myxda_OpenPortSuccessful(sender, e); }));
			}
			else
			{
				// Update the UI			
				switch (_state)
				{
				case States.CONNECTING:
					if (e.PortInfo.deviceId().isWirelessMaster())
					{
						// Set the label to indicate the ID of the station.
						labelStationId.Text = e.PortInfo.deviceId().toXsString().toString();
						_MyWirelessMasterDevice = _myxda.getDevice(e.PortInfo.deviceId());

						// Attach the callback handler. This causes events to arrive in m_myWirelessMasterCallback.
						_MyWirelessMasterDevice.addCallbackHandler(m_myWirelessMasterCallback);

						_state = States.CONNECTED;
						log(String.Format("Master Connected. Port: {0}, ID: {1}", e.PortInfo.portName().toString(), e.PortInfo.deviceId().toXsString().toString()));

						// Be sure to start with radio disabled
						if (_MyWirelessMasterDevice.isRadioEnabled())
						{
							SetRadioChannel(-1);
						}

						setWidgetsStates();
					}
					break;
				default:
					break;
				}
			}
		}
		void _myxda_OpenPortFailed(object sender, PortInfoArg e)
		{
			if (InvokeRequired)
			{
				// Update UI, make sure this happens on the UI thread
				BeginInvoke(new Action(delegate { _myxda_OpenPortFailed(sender, e); }));
			}
			else
			{
				if (e.PortInfo.deviceId().isWirelessMaster())
				{
					log(String.Format("Connect to wireless master failed. Port: {0}", e.PortInfo.portName().toString()));
				}
				else
				{
					log(String.Format("Connect to device failed. Port: {0}", e.PortInfo.portName().toString()));
				}

				switch (_state)
				{
					case States.CONNECTING:
						log("Closing XDA");
						_myxda.reset();
						_state = States.DETECTING;
						setWidgetsStates();
						break;
					default:
						break;
				}
			}
		}

        #region Callbacks
        void _callbackHandler_MtwWireless(object sender, DeviceIdArg e)
		{
			if (InvokeRequired)
			{
				// Update UI, make sure this happens on the UI thread
				BeginInvoke(new Action(delegate { _callbackHandler_MtwWireless(sender, e); }));
			}
			else
			{
				log(String.Format("MTw Connected. ID: {0}", e.DeviceId.toXsString().toString()));

                if (manager.checkDevice(e.DeviceId.toInt()))
                {
                    manager.connect(e.DeviceId.toInt());
                }

                displayOnList(e.DeviceId.toInt());
                btnMeasure.Enabled = (_state == States.ENABLED || _state == States.MEASURING) && connectedMtwList.Items.Count > 0;
			}
		}

		void _callbackHandler_MtwDisconnected(object sender, DeviceIdArg e)
		{
			if (InvokeRequired)
			{
				// Update UI, make sure this happens on the UI thread
				BeginInvoke(new Action(delegate { _callbackHandler_MtwDisconnected(sender, e); }));
			}
			else
			{
				String mtwIdStr = e.DeviceId.toInt().ToString();
				log(String.Format("MTw Disconnected. ID: {0}", mtwIdStr));
                manager.disconnect(e.DeviceId.toInt());
			}
		}

		void _callbackHandler_MeasurementStarted(object sender, DeviceIdArg e)
		{
			if (InvokeRequired)
			{
				// Update UI, make sure this happens on the UI thread
				BeginInvoke(new Action(delegate { _callbackHandler_MeasurementStarted(sender, e); }));
			}
			else
			{
				log(String.Format("Measurement Started. ID: {0}", e.DeviceId.toXsString().toString()));

				if (_myxda.getDevice(e.DeviceId).deviceId().toInt() == _MyWirelessMasterDevice.deviceId().toInt())
				{
					switch (_state)
					{
					case States.AWAIT_MEASUREMENT_START:
						{
							// Get the MTws that are measuring and attach callback handlers
							clearMeasuringMtws();
							List<XsDeviceId> deviceIds = m_myWirelessMasterCallback.getConnectedMtws();
							foreach (XsDeviceId devId in deviceIds)
							{
                                if (!alreadyKnowDevices.Contains(devId))
                                {
                                    alreadyKnowDevices.Add(devId);
                                    XsDevice mtw = _myxda.getDevice(devId);
                                    MyMtwCallback callback = new MyMtwCallback();

                                    // connect signals
                                    callback.DataAvailable += new EventHandler<DataAvailableArgs>(_callbackHandler_DataAvailable);
                                    callback.BatteryLevelChanged += new EventHandler<BatteryLevelChangedArgs>(_callbackHandler_BatteryLevelChanged);

                                    mtw.addCallbackHandler(callback);
                                    _measuringMtws[mtw] = callback;
                                }
							}
							_nextBatteryRequest = _measuringMtws.GetEnumerator();
							_state = States.MEASURING;
							setWidgetsStates();
						}
						break;

					default:
						break;
					}
				}
			}
		}
		void _callbackHandler_MeasurementStopped(object sender, DeviceIdArg e)
		{
			if (InvokeRequired)
			{
				// Update UI, make sure this happens on the UI thread
				BeginInvoke(new Action(delegate { _callbackHandler_MeasurementStopped(sender, e); }));
			}
			else
			{
                log(String.Format("Measurement Stopped. ID: {0}", e.DeviceId.toXsString().toString()));
				if (e.DeviceId .toInt()== _MyWirelessMasterDevice.deviceId().toInt())
                {
					clearMeasuringMtws();
					_state = States.OPERATIONAL;
					setWidgetsStates();
				}
			}
		}
		void _callbackHandler_DeviceError(object sender, DeviceErrorArgs e)
		{
			if (InvokeRequired)
			{
				// Update UI, make sure this happens on the UI thread
				BeginInvoke(new Action(delegate { _callbackHandler_DeviceError(sender, e); }));
			}
			else
			{
                if(_myxda.getDevice(e.DeviceId).deviceId().toInt() != _MyWirelessMasterDevice.deviceId().toInt())
				log(String.Format("ERROR. ID: {0}", e.DeviceId.toXsString().toString()));
				switch (_state)
				{
					case States.AWAIT_MEASUREMENT_START:
						_state = States.ENABLED;
						setWidgetsStates();
						break;

                    case States.MEASURING:
                    case States.SENDING:
                    {
                        manager.disconnect(e.DeviceId.toInt());
                        /*
                        ListViewItem disconnectedItem = connectedMtwList.FindItemWithText(e.DeviceId.toXsString().toString());
                        if (disconnectedItem != null && !_disconnectedIDs.ContainsKey(e.DeviceId.toInt()))
                        {
                            connectedMtwList.Items.Remove(disconnectedItem);
                            ListViewItem newItem = new ListViewItem(e.DeviceId.toXsString().toString());
                            newItem.BackColor = Color.Red;
                            connectedMtwList.Items.Add(newItem);
                            _connectedMtwData.Remove(e.DeviceId.toInt());
                            uint num = e.DeviceId.toInt();
                            string str = e.DeviceId.toXsString().toString();
                            _disconnectedIDs.Add(num, str);

                            connectedMtwListGroupBox.Text = String.Format("Connected MTw list ({0}):", connectedMtwList.Items.Count);
                            btnMeasure.Enabled = (_state == States.ENABLED || _state == States.MEASURING) && connectedMtwList.Items.Count > 0;
                        }
                         * */
                        break;
                    }
					default:
						break;
				}
			}
		}
		/*void _callbackHandler_WaitingForRecordingStart(object sender, DeviceIdArg e)
		{
			if (InvokeRequired)
			{
				// Update UI, make sure this happens on the UI thread
				BeginInvoke(new Action(delegate { _callbackHandler_WaitingForRecordingStart(sender, e); }));
			}
			else
			{
				log(String.Format("Waiting for recording start. ID: {0}", _MyWirelessMasterDevice.deviceId().toXsString().toString()));
				_state = States.AWAIT_RECORDING_START;
				setWidgetsStates();
			}
		}*/
		/*void _callbackHandler_RecordingStarted(object sender, DeviceIdArg e)
		{
			if (InvokeRequired)
			{
				// Update UI, make sure this happens on the UI thread
				BeginInvoke(new Action(delegate { _callbackHandler_RecordingStarted(sender, e); }));
			}
			else
			{
				if (_state == States.AWAIT_RECORDING_START)
				{
					log(String.Format("Waiting for recording start. ID: {0}", _MyWirelessMasterDevice.deviceId().toXsString().toString()));
					_state = States.RECORDING;
					setWidgetsStates();
				}
			}
		}*/
		/*void _callbackHandler_ProgressUpdate(object sender, ProgressUpdateArgs e)
		{
			if (InvokeRequired)
			{
				// Update UI, make sure this happens on the UI thread
				BeginInvoke(new Action(delegate { _callbackHandler_ProgressUpdate(sender, e); }));
			}
			else
			{
				if (_state == States.FLUSHING && e.Identifier == "Flushing")
				{
					if (comboBoxUpdateRate.SelectedIndex == 0)
					{
						// Nothing to flush when at the highest update rate.
						_MyWirelessMasterDevice.abortFlushing();
						log(String.Format("Flushing aborted. ID: {0}", _MyWirelessMasterDevice.deviceId().toXsString().toString()));
					}

					if (e.Total != 0 && comboBoxUpdateRate.SelectedIndex != 0)
					{
						// Only do this when there is still data to be flushed
						// and not the highest update rate was selected.
						progressBarFlushing.Maximum = e.Total;
						progressBarFlushing.Value = e.Current;
					}
				}
			}
		}*/
		void _callbackHandler_DataAvailable(object sender, DataAvailableArgs e)
		{
            DateTime currentTime = DateTime.Now;
            if(!manager.isKnown(e.Device.deviceId().toInt()))
                return;

            //if(currentTime.Subtract(manager.getData(e.Device.deviceId().toInt())._LastUpdate).TotalMilliseconds < 70)
             //   return;

            if(!(_state == States.MEASURING || _state == States.SENDING))
            {
                return;
            }
			if (InvokeRequired)
			{
				// Update UI, make sure this happens on the UI thread
				BeginInvoke(new Action(delegate { _callbackHandler_DataAvailable(sender, e); }));
			}
			else
			{
				String mtwIdStr = e.Device.deviceId().toInt().ToString();
                uint id = e.Device.deviceId().toInt();

                DeviceData newData = new DeviceData(manager.getData(e.Device.deviceId().toInt()));

                manager.connect(e.Device.deviceId().toInt());
                if (redDevices.Contains(e.Device.deviceId().toInt()))
                {
                    displayOnList(e.Device.deviceId().toInt());

                }
                if (connectedMtwList.FindItemWithText(mtwIdStr) == null)
				{
					log(String.Format("Obsolete data received of an MTw {0} that's no longer in the list.", mtwIdStr));
					return;
				}

				if (!e.Packet.containsSdiData())
				{
					log(String.Format("Packet received of an MTw {0} not containing data.", mtwIdStr));
					return;
				}

				// Getting SDI data.
				XsSdiData sdiData = e.Packet.sdiData();

                newData._rssi = e.Packet.rssi();
                newData._name = mtwIdStr;

                newData._Id = id;

                if (e.Packet.containsOrientation())
                {
                    //Getting Euler angles.
                    XsEuler oriEuler = e.Packet.orientationEuler();
                    XsQuaternion oriQuat = e.Packet.orientationQuaternion();
                    
                    // Just for fun: pitch to select.
                    // (you only want to select this in the GUI after the XKF-3w filters stabilized though)
                    /*if (checkBoxPitchToSelect.Checked == true && Math.Abs(oriEuler.y()) > 30)
                    {
                        connectedMtwList.SelectedItem = mtwIdStr;
                    }*/
                    newData._orientation = oriEuler;
                    newData._orientationQuat = oriQuat;
                }

                if (e.Packet.containsCalibratedGyroscopeData())
                {
                    //Getting Euler angles.
                    XsVector angVel = e.Packet.calibratedGyroscopeData();
                    
                    XsQuaternion oriQuat = e.Packet.orientationQuaternion();

                    newData._angularVel = angVel;
                    newData._orientationQuat = oriQuat;
                }


                if (e.Packet.containsCalibratedAcceleration())
                {
                    //Getting Euler angles.
                    XsVector acc = e.Packet.calibratedAcceleration();
                    newData._acceleration = acc;
                }

                if (e.Packet.containsCalibratedMagneticField())
                {
                    //Getting Euler angles.
                    XsVector mag = e.Packet.calibratedMagneticField();
                    newData._magneticF = mag;
                }
                
				// -- Determine effective update rate percentage --

				// Determine the number of frames over which the SDI data in this
				// packet was determined.
				int frameSkips;


				if (e.Packet.frameRange().last() > e.Packet.frameRange().first())
				{
					frameSkips = e.Packet.frameRange().last() - e.Packet.frameRange().first() - 1;
				}
				else
				{
					// Rollover (internal framecounter is unsigned 16 bits integer)
					frameSkips = 65535 + e.Packet.frameRange().last() - e.Packet.frameRange().first() - 1;
				}

                 //= new DateTime(Stopwatch.GetTimestamp());
                newData._freq = 1 / currentTime.Subtract(newData._LastUpdate).TotalMilliseconds * 1000;
                newData._LastUpdate = currentTime;
                newData._frameSkipsList.Add(frameSkips);
                newData._sumFrameSkips = newData._sumFrameSkips + (uint)frameSkips;
				newData._effectiveUpdateRate = (int)(100 * (1 - (float)newData._sumFrameSkips / (float)(newData._frameSkipsList.Count() + newData._sumFrameSkips)));

				while (newData._frameSkipsList.Count() + newData._sumFrameSkips > 99 && newData._frameSkipsList.Count() > 0)
				{
					newData._sumFrameSkips = newData._sumFrameSkips - (uint)newData._frameSkipsList[0];
					newData._frameSkipsList.RemoveAt(0);
				}

                manager.setData(e.Device.deviceId().toInt(), newData);

                if(currentTime.Subtract(TimeLastCheckDeco).TotalMilliseconds > 1000)
                {
                    manager.checkDisconnection(currentTime);
                    if(manager.isDisconnected())
                    {
                        List<uint> decos = manager.getDisconnectedDevices();
                        foreach (uint IMU in decos)
                        {
                            updateDisconnected(IMU);
                        }
                        displayWarning(true);
                    }
                    else
                    {
                        displayWarning(false);
                    }
                }
//Display selected Device
                if (connectedMtwList.SelectedItems != null)
				{
                    foreach (ListViewItem IMU in connectedMtwList.SelectedItems)
                        if (IMU.Text == mtwIdStr)
					        // Display data when MTw selected.

                            displayMtwData(e.Device.deviceId().toInt());
				}
//Send Data
                if (communicationActive && communicationPortIsOpen)
                {
                    if (SendAllOfThem)
                    {
                        if (!manager.updatedDevices.Contains(e.Device.deviceId().toInt()))
                        {
                            manager.updatedDevices.Add(e.Device.deviceId().toInt());
                        }
                        else
                        {
                            manager.updatedDevices.Clear();
                            manager.updatedDevices.Add(e.Device.deviceId().toInt());
                            byte[] result = new byte[manager.getConnectedDevices().Count * newData._dataBytes.Length];
                            int i = 0;
                            List<uint> coIMUids = manager.getConnectedDevices();
                            foreach (uint IMUid in coIMUids)
                            {
                                manager.getData(IMUid)._dataBytes.CopyTo(result, i);
                                i += newData._dataBytes.Length;
                            }
                            remoteCommunication.SendData(result);
                            LastSent = DateTime.Now;
                        }
                    }
                    else
                    {
                        remoteCommunication.SendData(newData._dataBytes);
                    }
                }
			}
		}

        void displayMtwData(uint IMUid)
		{
            DeviceData mtwData = manager.getData(IMUid);
			// Last known battery level.
			batteryLevelLabel.Text = String.Format("{0} [%]", mtwData._batteryLevel);


			// RSSI (received signals strenght indicator) of this packet.
			rssiLabel.Text = String.Format("{0} [dBm]", mtwData._freq);

			// Display Euler angles (if available).
            if (mtwData._containsOrientation)
            {
                rollLabel.Text = String.Format("{0,-5:f2} [deg]", mtwData._orientation.x());
                rollLabel.TextAlign = ContentAlignment.MiddleRight;
                pitchLabel.Text = String.Format("{0,-5:f2} [deg]", mtwData._orientation.y());
                yawLabel.Text = String.Format("{0,-5:f2} [deg]", mtwData._orientation.z());
            }

            labelIDInfo.Text = String.Format("{0}", (int)mtwData._Id);

			// Display effective update rate.
			effUpdateRateLabel.Text = String.Format("{0} [%]", mtwData._effectiveUpdateRate);
		}

		void _callbackHandler_BatteryLevelChanged(object sender, BatteryLevelChangedArgs e)
		{
			if (InvokeRequired)
			{
				// Update UI, make sure this happens on the UI thread
				BeginInvoke(new Action(delegate { _callbackHandler_BatteryLevelChanged(sender, e); }));
			}
			else
			{
				String mtwIdStr = e.DeviceId.toInt().ToString();
                if (connectedMtwList.FindItemWithText(mtwIdStr) == null)
				{
					log(String.Format("Obsolete data received of an MTw {0} that's no longer in the list.", mtwIdStr));
					return;
				}
                DeviceData data = new DeviceData(manager.getData(e.DeviceId.toInt()));
                data._batteryLevel = e.Level;
                manager.setData(e.DeviceId.toInt(), data);
                if (e.Level < batteryWarning)
                {
                    manager.LowBatt(e.DeviceId.toInt());
                    pictureBattery.Image = global::awindamonitor.Properties.Resources.LowBattery;
                }
			}
		}
		// Track connection changes
		void _callbackHandler_MtwConnectionChanged(object sender, MtwEventArgs e)
		{
			if (InvokeRequired)
			{
				// Update UI, make sure this happens on the UI thread
				BeginInvoke(new Action(delegate { _callbackHandler_MtwConnectionChanged(sender, e); }));
			}
			else
			{
				if (e.Connected)
				{
                    manager.checkDevice(e.Mtw.deviceId().toInt());
					//_mtws.Add(e.Mtw);
				}
                else if (manager.getConnectedDevices().Contains(e.Mtw.deviceId().toInt()))
				{
                    manager.disconnect(e.Mtw.deviceId().toInt());
					//_mtws.Remove(e.Mtw);
				}

				updateMTws();
			}
		}

		public void log(String message)
		{
			lock (logWindow)
			{
				if (logWindow.InvokeRequired)
				{
					logWindow.BeginInvoke(new Action(delegate { logWindow.AppendText(message + "\n"); }));
				}
				else
				{
					logWindow.AppendText(message + "\n");
					logWindow.ScrollToCaret();
				}				
			}
		}
        #endregion Callbacks

        #region GraphicList

        private void displayOnList(uint IMUid)
        {
            String mtwIdStr = IMUid.ToString();
            if (connectedMtwList.FindItemWithText(mtwIdStr) == null || redDevices.Contains(IMUid))
            {
                if(!greenDevices.Contains(IMUid))
                {
                    if (redDevices.Contains(IMUid))
                    {
                        connectedMtwList.Items.Remove(connectedMtwList.FindItemWithText(mtwIdStr));
                        redDevices.Remove(IMUid);
                    }
                    ListViewItem newItem = new ListViewItem(mtwIdStr);

                    connectedMtwList.Items.Add(newItem);
                    greenDevices.Add(IMUid);

                    connectedMtwListGroupBox.Text = String.Format("Connected MTw list ({0}):", connectedMtwList.Items.Count);
                }
            }
        }
        private void updateDisconnected(uint IMUid)
        {

            if (_state == States.MEASURING || _state == States.SENDING)
            {
                if (!redDevices.Contains(IMUid))
                {
                    turnRed(IMUid);
                }
            }
            else
            {
                removeFromList(IMUid);
            }
        }
        private void turnRed(uint IMUid)
        {
            ListViewItem disconnectedItem = connectedMtwList.FindItemWithText(IMUid.ToString());
            if (disconnectedItem != null)
            {
                connectedMtwList.Items.Remove(disconnectedItem);
                ListViewItem newItem = new ListViewItem(IMUid.ToString());
                newItem.BackColor = Color.Red;
                connectedMtwList.Items.Add(newItem);

                uint num = IMUid;
                string str = IMUid.ToString();
                redDevices.Add(IMUid);

                if (greenDevices.Contains(IMUid))
                {
                    greenDevices.Remove(IMUid);
                }

                connectedMtwListGroupBox.Text = String.Format("DisConnected MTw list ({0}):", connectedMtwList.Items.Count);
                btnMeasure.Enabled = (_state == States.ENABLED || _state == States.MEASURING) && connectedMtwList.Items.Count > 0;
            }
        }
        private void removeFromList(uint IMUid)
        {
            ListViewItem disconnectedItem = connectedMtwList.FindItemWithText(IMUid.ToString());
            connectedMtwList.Items.Remove(disconnectedItem);

            connectedMtwListGroupBox.Text = String.Format("Connected MTw list ({0}):", connectedMtwList.Items.Count);
            btnMeasure.Enabled = (_state == States.ENABLED || _state == States.MEASURING) && connectedMtwList.Items.Count > 0;
        }
        private void displayWarning(bool input)
        {
            if(input != warningDisplayed)
            {
                warningDisplayed = input;
                if(input)
                {
                    WarningIcon.Image = global::awindamonitor.Properties.Resources.Warning_sign;
                }
                else
                {
                    WarningIcon.Image = null;
                }
            }
        }
        #endregion GraphicList

        #region UI_Interaction
        private void updateMTws()
		{
			btnMeasure.Enabled = manager.isConnected();
		}
		private void clearMeasuringMtws()
		{
			lock(_measuringMtws)
			{
				foreach (KeyValuePair<XsDevice, MyMtwCallback> item in _measuringMtws)
				{
					item.Key.clearCallbackHandlers();
                    
				}
			}
			_measuringMtws.Clear();
			_nextBatteryRequest.Dispose();
		}
		private void Form1_FormClosed(object sender, FormClosedEventArgs e)
		{
			_portScanTimer.Enabled = false;
			_batteryLevelRequestTimer.Enabled = false;
            stopCommunication();clearMeasuringMtws();

			if (_MyWirelessMasterDevice != null)
				_MyWirelessMasterDevice.clearCallbackHandlers();
			m_myWirelessMasterCallback.Dispose();

			_myxda.Dispose();
			_myxda = null;
		}
		private void btnEnable_Click(object sender, EventArgs e)
		{
			SetRadioChannel(_state == States.CONNECTED ? Convert.ToInt16(comboBoxChannel.Text) : -1);
		}
		private void SetRadioChannel(int channel)
		{
			if (_MyWirelessMasterDevice.enableRadio(channel))
			{
				if (channel != -1) {
					log(String.Format("Master Enabled. ID: {0}, Channel: {1}",_MyWirelessMasterDevice.deviceId().toXsString().toString(),channel));

					// Supported update rates and maximum available from xda
					XsIntArray supportedRates = _MyWirelessMasterDevice.supportedUpdateRates();
					int maxUpdateRate = _MyWirelessMasterDevice.maximumUpdateRate();

					// -- Put the allowed update rates in the combobox for the user to choose from --
					comboBoxUpdateRate.Items.Clear();
					for (uint i = 0; i < supportedRates.size() && supportedRates.at(i) <= maxUpdateRate; ++i)
					{
						// This is an allowed update rate, so add it to the list.
						comboBoxUpdateRate.Items.Add(Convert.ToString(supportedRates.at(i)));
					}

					// Select the current update rate of the station.
					int updateRateIndex = comboBoxUpdateRate.FindString(Convert.ToString(_MyWirelessMasterDevice.updateRate()));
					comboBoxUpdateRate.SelectedIndex = updateRateIndex;

					_state = States.ENABLED;

					// Set a default update rate of 75 (if available) when we set the radio channel
					updateRateIndex = comboBoxUpdateRate.FindString(Convert.ToString(100));

                    if (updateRateIndex != -1)
                    {
                        comboBoxUpdateRate.SelectedIndex = updateRateIndex;
                    }
				}
				else 
				{
					log(String.Format("Master Disabled. ID: {0}",_MyWirelessMasterDevice.deviceId().toXsString().toString()));
					connectedMtwList.Items.Clear();
					_state = States.CONNECTED;
				}
				setWidgetsStates();
			}
			else
			{
				if (channel != -1) {
					log(String.Format("Failed to enable wireless master. ID: {0}, Channel: {1}",_MyWirelessMasterDevice.deviceId().toXsString().toString(), channel));
				}
				else {
					log(String.Format("Failed to disable wireless master. ID: {0}", _MyWirelessMasterDevice.deviceId().toXsString().toString()));
				}
			}
		}
		private void btnMeasure_Click(object sender, EventArgs e)
		{
			switch (_state) {
			case States.ENABLED:
			case States.OPERATIONAL:
			{
				// First set the update rate
				int desiredUpdateRate = Convert.ToInt32(comboBoxUpdateRate.Text);
				if (desiredUpdateRate != -1 && desiredUpdateRate != _MyWirelessMasterDevice.updateRate()) {
					if (_MyWirelessMasterDevice.setUpdateRate(desiredUpdateRate)) {
						log(String.Format("Update rate set. ID: {0}, Rate: {1}",_MyWirelessMasterDevice.deviceId().toXsString().toString(), desiredUpdateRate));
					}
					else {
						log(String.Format("Failed to set update rate. ID: {0}, Rate: {1}", _MyWirelessMasterDevice.deviceId().toXsString().toString(), desiredUpdateRate));
					}
				}

				if (comboBoxUpdateRate.SelectedIndex == 0)
				{
					MessageBox.Show("Note: at the highest update rate\nrecording will be at effective update rate.");
				}

                States bkpState = _state;
                // Set the state to AWAIT_MEASUREMENT_START and go to measurement
                _state = States.AWAIT_MEASUREMENT_START;

                if (_MyWirelessMasterDevice.gotoMeasurement())
                {
                    log(String.Format("Waiting for measurement start. ID: {0}", _MyWirelessMasterDevice.deviceId().toXsString().toString()));
                }
                else
                {
                    // If gotoMeasurement fails revert the state
                    _state = bkpState;
                }
				
			}
			break;

			case States.MEASURING:
			{
				if (_MyWirelessMasterDevice.gotoConfig()) {
                    connectedMtwList.Clear();
                    redDevices.Clear();
                    greenDevices.Clear();
                    
					log(String.Format("Stopping measurement. ID: {0}", _MyWirelessMasterDevice.deviceId().toXsString().toString()));
				}
				else {
					log(String.Format("Failed to stop measurement. ID: {0}", _MyWirelessMasterDevice.deviceId().toXsString().toString()));
				}
                if (communicationPortIsOpen)
                    stopCommunication();
			}
			break;
			default:
				break;

			}
			setWidgetsStates();     
		}
		//------------------------------------------------------------------------------
		// Set the states of widgets (labels, buttons and images) depending on the m_state.
		// This is basically a m_state machine with only 'm_state entry code'.
		//------------------------------------------------------------------------------
		private void setWidgetsStates()
		{
			switch (_state)
			{
				case Form1.States.DETECTING:
				{
                    //pictureBoxStateDiagram.Image = global::awindamonitor.Properties.Resources.connected;
                    pictureBox2.Image = null;
                    WarningIcon.Image = null;
                    pictureBattery.Image = null;
				} break;

				case Form1.States.CONNECTING:
				{
					btnEnable.Enabled = false;
                    //pictureBoxStateDiagram.Image = global::awindamonitor.Properties.Resources.connecting;
                    pictureBox2.Image = null;
					log("Scanning for station.");
                    buttonComm.Enabled = false;
                    textBoxAddress.Enabled = true;
                    textBoxPort.Enabled = true;
                    WarningIcon.Image = null;
                    pictureBattery.Image = null;
				} break;

				case Form1.States.CONNECTED:
				{
					btnMeasure.Enabled = false;
					labelChannel.Enabled = true;
					comboBoxChannel.Enabled = true;
					btnEnable.Enabled = true;
					btnEnable.Text = "Enable";
					labelUpdateRate.Enabled = false;
					comboBoxUpdateRate.Enabled = false;
                    //pictureBoxStateDiagram.Image = global::awindamonitor.Properties.Resources.connected;
                    pictureBox2.Image = null;
                    buttonComm.Enabled = false;
                    //buttonResetOrientation.Enabled = false;
                    //resetButtonSelected.Enabled = false;
                    pictureOrientation.Enabled = false;
                    textBoxAddress.Enabled = true;
                    textBoxPort.Enabled = true;
                    WarningIcon.Image = null;
                    pictureBattery.Image = null;
				} break;

				case Form1.States.ENABLED:
				{
					btnMeasure.Enabled = connectedMtwList.Items.Count > 0;
					btnMeasure.Text = "Start Measurement";
					btnEnable.Text = "Disable";
					btnEnable.Enabled = true;
					labelChannel.Enabled = false;
					comboBoxChannel.Enabled = false;
					labelUpdateRate.Enabled = true;
					comboBoxUpdateRate.Enabled = true;
                    //pictureBoxStateDiagram.Image = global::awindamonitor.Properties.Resources.enabled;
                    pictureBox2.Image = global::awindamonitor.Properties.Resources.XSens_Unit_Move;
                    buttonComm.Enabled = false;
                    //buttonResetOrientation.Enabled = false;
                    //resetButtonSelected.Enabled = false;
                    pictureOrientation.Enabled = false;
                    log("Please shake the units you want to use.");
                    textBoxAddress.Enabled = true;
                    textBoxPort.Enabled = true;
                    WarningIcon.Image = null;
                    pictureBattery.Image = null;
				} break;

				case Form1.States.OPERATIONAL:
				{
					btnMeasure.Enabled = connectedMtwList.Items.Count > 0;
                    btnMeasure.Text = "Start Measurement";
                    buttonComm.Enabled = false;
                    buttonComm.Text = "Start Communication";
					btnEnable.Text = "Disable";
					btnEnable.Enabled = true;
					labelChannel.Enabled = false;
					comboBoxChannel.Enabled = false;
					labelUpdateRate.Enabled = true;
					comboBoxUpdateRate.Enabled = true;
                    //pictureBoxStateDiagram.Image = global::awindamonitor.Properties.Resources.operational;
                    //pictureBox2.Image = global::awindamonitor.Properties.Resources.XSens_Unit_Move;
                    pictureBox2.Image = null;
                    //buttonResetOrientation.Enabled = false;
                    //resetButtonSelected.Enabled = false;
                    pictureOrientation.Enabled = false;
                    textBoxAddress.Enabled = true;
                    textBoxPort.Enabled = true;
                    WarningIcon.Image = null;
                    pictureBattery.Image = null;
                    manager.reset();
				} break;

				case Form1.States.AWAIT_MEASUREMENT_START:
				{
                    btnEnable.Enabled = false;
					btnMeasure.Text = "Waiting for start...";
					btnMeasure.Enabled = false;
					labelUpdateRate.Enabled = false;
					comboBoxUpdateRate.Enabled = false;
                    //pictureBoxStateDiagram.Image = global::awindamonitor.Properties.Resources.await_measurement_start;
                    pictureBox2.Image = global::awindamonitor.Properties.Resources.XSens_Unit_Dont_Touch;
                    textBoxAddress.Enabled = true;
                    textBoxPort.Enabled = true;
                    WarningIcon.Image = null;
                    pictureBattery.Image = null;
				} break;

				case Form1.States.MEASURING:
				{
					btnEnable.Enabled = false;
                    btnMeasure.Text = "Stop measuring";
                    buttonComm.Enabled = true;
                    buttonComm.Text = "Start Communication";
					btnMeasure.Enabled = true;
					labelUpdateRate.Enabled = false;
					comboBoxUpdateRate.Enabled = false;
                    //pictureBoxStateDiagram.Image = global::awindamonitor.Properties.Resources.measuring;
                    pictureBox2.Image = null;
                    buttonComm.Enabled = true;
                    //buttonResetOrientation.Enabled = true;
                    //resetButtonSelected.Enabled = true;
                    pictureOrientation.Enabled = true;
                    textBoxAddress.Enabled = true;
                    textBoxPort.Enabled = true;
                    WarningIcon.Image = null;
				} break;

                case Form1.States.SENDING:
                {
                    buttonComm.Enabled = true;
                    buttonComm.Text = "Stop Communication";
                    textBoxAddress.Enabled = false;
                    textBoxPort.Enabled = false;
                    btnMeasure.Enabled = false;
                    //pictureBoxStateDiagram.Image = global::awindamonitor.Properties.Resources.recording;
                    pictureBox2.Image = null;
                } break;

				default:
						break;
			}
		}

        private void buttonComm_Click(object sender, EventArgs e)
        {
            if (_state == States.SENDING) //communicationActive
            {
                stopCommunication();
                _state = States.MEASURING;
                setWidgetsStates();
            }
            else
            {
                startCommunication();
            }
        }
        public void startCommunication()
        {
            if (remoteCommunication != null)
                remoteCommunication.CloseSocket();

            if ((remoteCommunication as UDPCommunication) == null)
            {
                remoteCommunication = new UDPCommunication(textBoxAddress.Text, int.Parse(textBoxPort.Text));
                (remoteCommunication as UDPCommunication).FastReadIncreasedCPUload = this.UDPfastRead;
            }
            //remoteCommunication.CommunicationDataArrived -= new CommunicationSocket.CommunicationDataArrivedDel(remoteComm_CommunicationDataArrived);
            //remoteCommunication.CommunicationDataArrived += new CommunicationSocket.CommunicationDataArrivedDel(remoteComm_CommunicationDataArrived);
            communicationPortIsOpen = remoteCommunication.OpenSocket();
            communicationActive = true;
            log("Communication Started");
            _state = States.SENDING;
            setWidgetsStates();    
        }
        /// <summary>
        /// Stops the communication for inter-app communication
        /// </summary>
        public void stopCommunication()
        {
            if (remoteCommunication != null)
            {
                communicationPortIsOpen = !remoteCommunication.CloseSocket();
                remoteCommunication = null;
            }
            communicationActive = false;
            log("Communication Closed");
            //buttonComm.Text = "STRAT Comm";
        }
        private void buttonResetOrientation_Click(object sender, EventArgs e)
        {
            List<XsDeviceId> deviceIds = m_myWirelessMasterCallback.getConnectedMtws();
            foreach (XsDeviceId devId in deviceIds)
            {
                XsDevice mtw = _myxda.getDevice(devId);
                mtw.resetOrientation(XsResetMethod.XRM_Alignment);
            }
        }
        private void resetButtonSelected_Click(object sender, EventArgs e)
        {
            List<XsDeviceId> deviceIds = m_myWirelessMasterCallback.getConnectedMtws();
            foreach (XsDeviceId devId in deviceIds)
            {
                String mtwIdStr = devId.toInt().ToString();
                if (connectedMtwList.Text == mtwIdStr)
                {
                    XsDevice mtw = _myxda.getDevice(devId);
                    mtw.resetOrientation(XsResetMethod.XRM_Alignment);
                }
            }
        }

        /* private void pictureBox1_Click(object sender, EventArgs e)
         {

         }*/

        #endregion UI_Interaction
    }
}
