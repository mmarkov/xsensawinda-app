/*	Copyright (c) 2003-2016 Xsens Technologies B.V. or subsidiaries worldwide.
	All rights reserved.

	Redistribution and use in source and binary forms, with or without modification,
	are permitted provided that the following conditions are met:

	1.	Redistributions of source code must retain the above copyright notice,
		this list of conditions and the following disclaimer.

	2.	Redistributions in binary form must reproduce the above copyright notice,
		this list of conditions and the following disclaimer in the documentation
		and/or other materials provided with the distribution.

	3.	Neither the names of the copyright holders nor the names of their contributors
		may be used to endorse or promote products derived from this software without
		specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
	THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
	OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
	HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY OR
	TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

﻿namespace AwindaMonitor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnEnable = new System.Windows.Forms.Button();
            this.btnMeasure = new System.Windows.Forms.Button();
            this.labelDevId = new System.Windows.Forms.Label();
            this.labelDeviceId = new System.Windows.Forms.Label();
            this.comboBoxChannel = new System.Windows.Forms.ComboBox();
            this.labelChannel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxPort = new System.Windows.Forms.TextBox();
            this.textBoxAddress = new System.Windows.Forms.TextBox();
            this.buttonComm = new System.Windows.Forms.Button();
            this.labelStationId = new System.Windows.Forms.Label();
            this.comboBoxUpdateRate = new System.Windows.Forms.ComboBox();
            this.labelUpdateRate = new System.Windows.Forms.Label();
            this.dockedMtwListGroupBox = new System.Windows.Forms.GroupBox();
            this.dockedMtwList = new System.Windows.Forms.ListBox();
            this.connectedMtwListGroupBox = new System.Windows.Forms.GroupBox();
            this.connectedMtwList = new System.Windows.Forms.ListView();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.logWindow = new System.Windows.Forms.RichTextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.labelIDInfo = new System.Windows.Forms.Label();
            this.labelID = new System.Windows.Forms.Label();
            this.yawLabel = new System.Windows.Forms.Label();
            this.pitchLabel = new System.Windows.Forms.Label();
            this.rollLabel = new System.Windows.Forms.Label();
            this.effUpdateRateLabel = new System.Windows.Forms.Label();
            this.rssiLabel = new System.Windows.Forms.Label();
            this.batteryLevelLabel = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBattery = new System.Windows.Forms.PictureBox();
            this.WarningIcon = new System.Windows.Forms.PictureBox();
            this.pictureOrientation = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            this.dockedMtwListGroupBox.SuspendLayout();
            this.connectedMtwListGroupBox.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBattery)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WarningIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureOrientation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // btnEnable
            // 
            this.btnEnable.Enabled = false;
            this.btnEnable.Location = new System.Drawing.Point(18, 76);
            this.btnEnable.Name = "btnEnable";
            this.btnEnable.Size = new System.Drawing.Size(144, 33);
            this.btnEnable.TabIndex = 4;
            this.btnEnable.Text = "Enable";
            this.btnEnable.UseVisualStyleBackColor = true;
            this.btnEnable.Click += new System.EventHandler(this.btnEnable_Click);
            // 
            // btnMeasure
            // 
            this.btnMeasure.Enabled = false;
            this.btnMeasure.Location = new System.Drawing.Point(18, 163);
            this.btnMeasure.Name = "btnMeasure";
            this.btnMeasure.Size = new System.Drawing.Size(144, 33);
            this.btnMeasure.TabIndex = 7;
            this.btnMeasure.Text = "Start Measuring";
            this.btnMeasure.UseVisualStyleBackColor = true;
            this.btnMeasure.Click += new System.EventHandler(this.btnMeasure_Click);
            // 
            // labelDevId
            // 
            this.labelDevId.AutoSize = true;
            this.labelDevId.Location = new System.Drawing.Point(15, 18);
            this.labelDevId.Name = "labelDevId";
            this.labelDevId.Size = new System.Drawing.Size(21, 13);
            this.labelDevId.TabIndex = 13;
            this.labelDevId.Text = "ID:";
            // 
            // labelDeviceId
            // 
            this.labelDeviceId.AutoSize = true;
            this.labelDeviceId.Location = new System.Drawing.Point(41, 30);
            this.labelDeviceId.Name = "labelDeviceId";
            this.labelDeviceId.Size = new System.Drawing.Size(0, 13);
            this.labelDeviceId.TabIndex = 14;
            // 
            // comboBoxChannel
            // 
            this.comboBoxChannel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxChannel.Enabled = false;
            this.comboBoxChannel.FormattingEnabled = true;
            this.comboBoxChannel.Items.AddRange(new object[] {
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25"});
            this.comboBoxChannel.Location = new System.Drawing.Point(87, 40);
            this.comboBoxChannel.Name = "comboBoxChannel";
            this.comboBoxChannel.Size = new System.Drawing.Size(75, 21);
            this.comboBoxChannel.TabIndex = 15;
            // 
            // labelChannel
            // 
            this.labelChannel.AutoSize = true;
            this.labelChannel.Enabled = false;
            this.labelChannel.Location = new System.Drawing.Point(15, 43);
            this.labelChannel.Name = "labelChannel";
            this.labelChannel.Size = new System.Drawing.Size(49, 13);
            this.labelChannel.TabIndex = 16;
            this.labelChannel.Text = "Channel:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBoxPort);
            this.groupBox1.Controls.Add(this.textBoxAddress);
            this.groupBox1.Controls.Add(this.buttonComm);
            this.groupBox1.Controls.Add(this.labelStationId);
            this.groupBox1.Controls.Add(this.comboBoxUpdateRate);
            this.groupBox1.Controls.Add(this.labelUpdateRate);
            this.groupBox1.Controls.Add(this.labelDevId);
            this.groupBox1.Controls.Add(this.comboBoxChannel);
            this.groupBox1.Controls.Add(this.labelChannel);
            this.groupBox1.Controls.Add(this.btnEnable);
            this.groupBox1.Controls.Add(this.btnMeasure);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(176, 351);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Wireless master properties:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 209);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 13);
            this.label1.TabIndex = 29;
            this.label1.Text = "Remote Address and Port";
            // 
            // textBoxPort
            // 
            this.textBoxPort.Location = new System.Drawing.Point(90, 232);
            this.textBoxPort.Name = "textBoxPort";
            this.textBoxPort.Size = new System.Drawing.Size(72, 20);
            this.textBoxPort.TabIndex = 27;
            this.textBoxPort.Text = "8060";
            // 
            // textBoxAddress
            // 
            this.textBoxAddress.Location = new System.Drawing.Point(21, 232);
            this.textBoxAddress.Name = "textBoxAddress";
            this.textBoxAddress.Size = new System.Drawing.Size(63, 20);
            this.textBoxAddress.TabIndex = 26;
            this.textBoxAddress.Text = "127.0.0.1";
            // 
            // buttonComm
            // 
            this.buttonComm.Enabled = false;
            this.buttonComm.Location = new System.Drawing.Point(21, 263);
            this.buttonComm.Name = "buttonComm";
            this.buttonComm.Size = new System.Drawing.Size(142, 33);
            this.buttonComm.TabIndex = 25;
            this.buttonComm.Text = "START Comm";
            this.buttonComm.UseVisualStyleBackColor = true;
            this.buttonComm.Click += new System.EventHandler(this.buttonComm_Click);
            // 
            // labelStationId
            // 
            this.labelStationId.AutoSize = true;
            this.labelStationId.Location = new System.Drawing.Point(87, 18);
            this.labelStationId.Name = "labelStationId";
            this.labelStationId.Size = new System.Drawing.Size(28, 13);
            this.labelStationId.TabIndex = 23;
            this.labelStationId.Text = "       ";
            // 
            // comboBoxUpdateRate
            // 
            this.comboBoxUpdateRate.Enabled = false;
            this.comboBoxUpdateRate.FormattingEnabled = true;
            this.comboBoxUpdateRate.Location = new System.Drawing.Point(87, 125);
            this.comboBoxUpdateRate.Name = "comboBoxUpdateRate";
            this.comboBoxUpdateRate.Size = new System.Drawing.Size(75, 21);
            this.comboBoxUpdateRate.TabIndex = 18;
            // 
            // labelUpdateRate
            // 
            this.labelUpdateRate.AutoSize = true;
            this.labelUpdateRate.Enabled = false;
            this.labelUpdateRate.Location = new System.Drawing.Point(18, 129);
            this.labelUpdateRate.Name = "labelUpdateRate";
            this.labelUpdateRate.Size = new System.Drawing.Size(66, 13);
            this.labelUpdateRate.TabIndex = 17;
            this.labelUpdateRate.Text = "Update rate:";
            // 
            // dockedMtwListGroupBox
            // 
            this.dockedMtwListGroupBox.Controls.Add(this.dockedMtwList);
            this.dockedMtwListGroupBox.Location = new System.Drawing.Point(208, 12);
            this.dockedMtwListGroupBox.Name = "dockedMtwListGroupBox";
            this.dockedMtwListGroupBox.Size = new System.Drawing.Size(185, 125);
            this.dockedMtwListGroupBox.TabIndex = 18;
            this.dockedMtwListGroupBox.TabStop = false;
            this.dockedMtwListGroupBox.Text = "Docked Mtw list (0):";
            // 
            // dockedMtwList
            // 
            this.dockedMtwList.FormattingEnabled = true;
            this.dockedMtwList.Location = new System.Drawing.Point(7, 18);
            this.dockedMtwList.Name = "dockedMtwList";
            this.dockedMtwList.Size = new System.Drawing.Size(162, 95);
            this.dockedMtwList.TabIndex = 0;
            // 
            // connectedMtwListGroupBox
            // 
            this.connectedMtwListGroupBox.Controls.Add(this.connectedMtwList);
            this.connectedMtwListGroupBox.Location = new System.Drawing.Point(208, 141);
            this.connectedMtwListGroupBox.Name = "connectedMtwListGroupBox";
            this.connectedMtwListGroupBox.Size = new System.Drawing.Size(185, 210);
            this.connectedMtwListGroupBox.TabIndex = 19;
            this.connectedMtwListGroupBox.TabStop = false;
            this.connectedMtwListGroupBox.Text = "Connected Mtw list (0):";
            // 
            // connectedMtwList
            // 
            this.connectedMtwList.Location = new System.Drawing.Point(17, 23);
            this.connectedMtwList.Name = "connectedMtwList";
            this.connectedMtwList.Size = new System.Drawing.Size(142, 168);
            this.connectedMtwList.TabIndex = 25;
            this.connectedMtwList.UseCompatibleStateImageBehavior = false;
            this.connectedMtwList.View = System.Windows.Forms.View.SmallIcon;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.logWindow);
            this.groupBox4.Location = new System.Drawing.Point(400, 13);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(284, 141);
            this.groupBox4.TabIndex = 20;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "What\'s going on:";
            // 
            // logWindow
            // 
            this.logWindow.Location = new System.Drawing.Point(12, 19);
            this.logWindow.Name = "logWindow";
            this.logWindow.Size = new System.Drawing.Size(258, 116);
            this.logWindow.TabIndex = 7;
            this.logWindow.Text = "";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.labelIDInfo);
            this.groupBox5.Controls.Add(this.labelID);
            this.groupBox5.Controls.Add(this.yawLabel);
            this.groupBox5.Controls.Add(this.pitchLabel);
            this.groupBox5.Controls.Add(this.rollLabel);
            this.groupBox5.Controls.Add(this.effUpdateRateLabel);
            this.groupBox5.Controls.Add(this.rssiLabel);
            this.groupBox5.Controls.Add(this.batteryLevelLabel);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Location = new System.Drawing.Point(400, 156);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(161, 207);
            this.groupBox5.TabIndex = 21;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Selected Mtw properties:";
            // 
            // labelIDInfo
            // 
            this.labelIDInfo.AutoSize = true;
            this.labelIDInfo.Location = new System.Drawing.Point(92, 186);
            this.labelIDInfo.Name = "labelIDInfo";
            this.labelIDInfo.Size = new System.Drawing.Size(10, 13);
            this.labelIDInfo.TabIndex = 13;
            this.labelIDInfo.Text = "-";
            this.labelIDInfo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelID
            // 
            this.labelID.AutoSize = true;
            this.labelID.Location = new System.Drawing.Point(9, 186);
            this.labelID.Name = "labelID";
            this.labelID.Size = new System.Drawing.Size(21, 13);
            this.labelID.TabIndex = 12;
            this.labelID.Text = "ID:";
            // 
            // yawLabel
            // 
            this.yawLabel.AutoSize = true;
            this.yawLabel.Location = new System.Drawing.Point(92, 160);
            this.yawLabel.Name = "yawLabel";
            this.yawLabel.Size = new System.Drawing.Size(10, 13);
            this.yawLabel.TabIndex = 11;
            this.yawLabel.Text = "-";
            this.yawLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pitchLabel
            // 
            this.pitchLabel.AutoSize = true;
            this.pitchLabel.Location = new System.Drawing.Point(92, 132);
            this.pitchLabel.Name = "pitchLabel";
            this.pitchLabel.Size = new System.Drawing.Size(10, 13);
            this.pitchLabel.TabIndex = 10;
            this.pitchLabel.Text = "-";
            this.pitchLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // rollLabel
            // 
            this.rollLabel.AutoSize = true;
            this.rollLabel.Location = new System.Drawing.Point(92, 104);
            this.rollLabel.Name = "rollLabel";
            this.rollLabel.Size = new System.Drawing.Size(10, 13);
            this.rollLabel.TabIndex = 9;
            this.rollLabel.Text = "-";
            this.rollLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // effUpdateRateLabel
            // 
            this.effUpdateRateLabel.AutoSize = true;
            this.effUpdateRateLabel.Location = new System.Drawing.Point(92, 76);
            this.effUpdateRateLabel.Name = "effUpdateRateLabel";
            this.effUpdateRateLabel.Size = new System.Drawing.Size(10, 13);
            this.effUpdateRateLabel.TabIndex = 8;
            this.effUpdateRateLabel.Text = "-";
            this.effUpdateRateLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // rssiLabel
            // 
            this.rssiLabel.AutoSize = true;
            this.rssiLabel.Location = new System.Drawing.Point(92, 48);
            this.rssiLabel.Name = "rssiLabel";
            this.rssiLabel.Size = new System.Drawing.Size(10, 13);
            this.rssiLabel.TabIndex = 7;
            this.rssiLabel.Text = "-";
            this.rssiLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // batteryLevelLabel
            // 
            this.batteryLevelLabel.AutoSize = true;
            this.batteryLevelLabel.Location = new System.Drawing.Point(92, 20);
            this.batteryLevelLabel.Name = "batteryLevelLabel";
            this.batteryLevelLabel.Size = new System.Drawing.Size(10, 13);
            this.batteryLevelLabel.TabIndex = 6;
            this.batteryLevelLabel.Text = "-";
            this.batteryLevelLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 160);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(31, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "Yaw:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 132);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(34, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Pitch:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 104);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(28, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Roll:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 76);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Eff. update rate:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 48);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Freq:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Battery level:";
            // 
            // pictureBattery
            // 
            this.pictureBattery.Location = new System.Drawing.Point(710, 166);
            this.pictureBattery.Name = "pictureBattery";
            this.pictureBattery.Size = new System.Drawing.Size(128, 63);
            this.pictureBattery.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBattery.TabIndex = 31;
            this.pictureBattery.TabStop = false;
            // 
            // WarningIcon
            // 
            this.WarningIcon.Location = new System.Drawing.Point(569, 167);
            this.WarningIcon.Name = "WarningIcon";
            this.WarningIcon.Size = new System.Drawing.Size(128, 63);
            this.WarningIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.WarningIcon.TabIndex = 25;
            this.WarningIcon.TabStop = false;
            // 
            // pictureOrientation
            // 
            this.pictureOrientation.Image = global::awindamonitor.Properties.Resources.MTx_3_DOF_orientation_tracker;
            this.pictureOrientation.Location = new System.Drawing.Point(690, 33);
            this.pictureOrientation.Name = "pictureOrientation";
            this.pictureOrientation.Size = new System.Drawing.Size(148, 109);
            this.pictureOrientation.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureOrientation.TabIndex = 30;
            this.pictureOrientation.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(567, 246);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(271, 89);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 24;
            this.pictureBox2.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(850, 371);
            this.Controls.Add(this.pictureBattery);
            this.Controls.Add(this.WarningIcon);
            this.Controls.Add(this.pictureOrientation);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.connectedMtwListGroupBox);
            this.Controls.Add(this.dockedMtwListGroupBox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.labelDeviceId);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.dockedMtwListGroupBox.ResumeLayout(false);
            this.connectedMtwListGroupBox.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBattery)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WarningIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureOrientation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnEnable;
        private System.Windows.Forms.Button btnMeasure;
        private System.Windows.Forms.Label labelDevId;
        private System.Windows.Forms.Label labelDeviceId;
        private System.Windows.Forms.ComboBox comboBoxChannel;
        private System.Windows.Forms.Label labelChannel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelUpdateRate;
        private System.Windows.Forms.ComboBox comboBoxUpdateRate;
        private System.Windows.Forms.GroupBox dockedMtwListGroupBox;
        private System.Windows.Forms.GroupBox connectedMtwListGroupBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RichTextBox logWindow;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label yawLabel;
        private System.Windows.Forms.Label pitchLabel;
        private System.Windows.Forms.Label rollLabel;
        private System.Windows.Forms.Label effUpdateRateLabel;
        private System.Windows.Forms.Label rssiLabel;
        private System.Windows.Forms.Label batteryLevelLabel;
        private System.Windows.Forms.Label labelStationId;
        private System.Windows.Forms.ListBox dockedMtwList;
        private System.Windows.Forms.TextBox textBoxPort;
        private System.Windows.Forms.TextBox textBoxAddress;
        private System.Windows.Forms.Button buttonComm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureOrientation;
        private System.Windows.Forms.Label labelIDInfo;
        private System.Windows.Forms.Label labelID;
        private System.Windows.Forms.ListView connectedMtwList;
        private System.Windows.Forms.PictureBox WarningIcon;
        private System.Windows.Forms.PictureBox pictureBattery;
    }
}

