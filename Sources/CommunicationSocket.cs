﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.ComponentModel;
using System.Diagnostics;

namespace AwindaMonitor
{
    public abstract class CommunicationSocket
    {
        public delegate void CommunicationDataArrivedDel(byte[] data);
        public event CommunicationDataArrivedDel CommunicationDataArrived;

        public virtual bool OpenSocket()
        {
            return false;
        }
        public virtual bool CloseSocket()
        {
            return false;
        }
        public virtual void SendData(byte[] data)
        {
        }

        public void ProcessReceivedData(byte[] receivedData)
        {
            if (CommunicationDataArrived != null)
                CommunicationDataArrived(receivedData);

        }
    }

    public class UDPCommunication : CommunicationSocket
    {
        UdpClient udpClient;
        IPEndPoint ipEndPoint;
        byte[] receivedByteArray;
        private bool fastReadIncreasedCPUload = true;
        public bool FastReadIncreasedCPUload
        {
            get { return fastReadIncreasedCPUload; }
            set { fastReadIncreasedCPUload = value; }
        }

        //private static UDPCommunication instance;
        //public static UDPCommunication Instance { get { return instance; } }
        //private bool listen = false;
        Socket senderSocket;
        IPEndPoint ipEndPointSend;
        int receiverPort;
        int senderPort;
        string senderHost;
        int iter = 0;
        public UDPCommunication(string host, int port) //8051
        {

            //udpClient = new UdpClient(port);
            //ipEndPoint =  new IPEndPoint(IPAddress.Any, port);

            //senderSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            //ipEndPointSend = new IPEndPoint(IPAddress.Parse("127.0.0.1"), port+1);
            receiverPort = port - 1;
            senderPort = port;
            senderHost = host;
        }



        public override bool OpenSocket()
        {
            try
            {
                udpClient = new UdpClient(receiverPort);
                ipEndPoint = new IPEndPoint(IPAddress.Any, receiverPort);

                senderSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                ipEndPointSend = new IPEndPoint(IPAddress.Parse(senderHost), senderPort);
                udpClient.BeginReceive(DataReceived, udpClient);
            }
            catch { }
            //listen = true;
            return true;
        }

        private void DataReceived(IAsyncResult ar)
        {
            //lock (lock_)
            {
                UdpClient c = (UdpClient)ar.AsyncState;
                IPEndPoint receivedIpEndPoint = new IPEndPoint(IPAddress.Any, 0);

                try
                {
                    receivedByteArray = c.EndReceive(ar, ref receivedIpEndPoint);
                    base.ProcessReceivedData(receivedByteArray);
                    // Restart listening for udp data packages
                    c.BeginReceive(DataReceived, ar.AsyncState);
                }
                catch { }
            }
        }


        public override bool CloseSocket()
        {
            //listen = false;
            //listener.EndReceive(res, ref groupEP);
            if (udpClient != null)
                udpClient.Close();

            if (senderSocket != null)
                senderSocket.Close();

            return true;
        }
        bool isSending = false;
        public override void SendData(byte[] data)
        {
            if (isSending)
                return;
            isSending = true;
            senderSocket.SendTo(data, ipEndPointSend);
            Debug.WriteLine(iter++);
            isSending = false;
        }
    }

    public class SerialCommunication : CommunicationSocket
    {

        SerialPort serialPort;
        private Queue<byte> recievedData = new Queue<byte>();
        public SerialCommunication(string portName, int baudRate)
        {
            serialPort = new SerialPort(portName, baudRate);
            serialPort.DataReceived += new SerialDataReceivedEventHandler(serialPort_DataReceived);
        }

        void serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            byte[] data = new byte[serialPort.BytesToRead];
            serialPort.Read(data, 0, data.Length);

            data.ToList().ForEach(b => recievedData.Enqueue(b));
            base.ProcessReceivedData(data.ToArray());
        }

        public override bool OpenSocket()
        {
            serialPort.Open();

            while (!serialPort.IsOpen)
            {
                Thread.Sleep(15);
                System.Windows.Forms.Application.DoEvents();
                //implement timer that checks after some time that socket coudlnt be opened
            }

            return true;
        }

        public override bool CloseSocket()
        {
            if (serialPort != null && serialPort.IsOpen)
                serialPort.Close();
            while (serialPort.IsOpen)
            {
                Thread.Sleep(15);
                System.Windows.Forms.Application.DoEvents();
                //implement timer that checks after some time that socket coudnt be opened
            }

            return true;
        }
        public override void SendData(byte[] data)
        {
            if (serialPort.IsOpen)
                serialPort.Write(data, 0, data.Length);
        }
    }
}
