# XSensAwinda-App

XSens app collects the data from XSens Awinda IMUs in real time and transmits it to the designated UDP port. 
This allows for simple communication between IMU sensors and external programs.